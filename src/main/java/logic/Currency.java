package logic;

public enum Currency {

    RUB("RUB"), USD("USD"), EUR("EUR"), CHF("CHF"), GBP("GBP"), JPY("JPY");

    private String value;

    Currency(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
