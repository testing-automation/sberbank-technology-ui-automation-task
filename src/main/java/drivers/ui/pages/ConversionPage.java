package drivers.ui.pages;

import configuration.Configuration;
import drivers.ui.WebDriverManager;
import io.qameta.allure.Step;
import logic.Currency;
import org.aeonbits.owner.ConfigFactory;
import org.awaitility.Awaitility;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ConversionPage {

    private WebDriver webDriver;
    private Configuration configuration = ConfigFactory.create(Configuration.class);

    public ConversionPage() {
        webDriver = new WebDriverManager().getWebDriver();
    }

    @Step
    public ConversionPage open() {
        webDriver.get(configuration.getBaseUrl() + "/ru/quotes/converter");
        return this;
    }

    @Step
    public ConversionPage chooseFromCurrency(Currency currencyFrom) {
        String converterFromLocator = "//select[@name='converterFrom']/..";
        webDriver.findElement(By.xpath(converterFromLocator)).click();
        webDriver.findElement(By.xpath(converterFromLocator + String.format("//span[text()='%s']", currencyFrom.getValue()))).click();
        return this;
    }

    @Step
    public ConversionPage chooseToCurrency(Currency currencyTo) {
        String converterToLocator = "//select[@name='converterTo']/..";
        webDriver.findElement(By.xpath(converterToLocator)).click();
        webDriver.findElement(By.xpath(converterToLocator + String.format("//span[text()='%s']", currencyTo.getValue()))).click();
        return this;
    }

    @Step
    public ConversionPage fillAmount(double amountForConversion) {
        Awaitility.await("Wait for sum field is not empty").until(() -> !getAmountFieldWebElement().getAttribute("value").isEmpty());
        getAmountFieldWebElement().clear();
        getAmountFieldWebElement().sendKeys(new Double(amountForConversion).toString());
        return this;
    }

    @Step
    private WebElement getAmountFieldWebElement() {
        return webDriver.findElement(By.xpath("//input[@placeholder='Сумма']"));
    }

    @Step
    public double getSaleRate() {
        String sellRateText = webDriver.findElement(By.xpath("//td[@class='rates-current__table-cell rates-current__table-cell_column_sell']//span[@class='rates-current__rate-value']"))
                .getText().replace(",", ".");
        return Double.parseDouble(sellRateText);
    }

    @Step
    public double getBuyRate() {
        String sellRateText = webDriver.findElement(By.xpath("//td[@class='rates-current__table-cell rates-current__table-cell_column_buy']//span[@class='rates-current__rate-value']"))
                .getText().replace(",", ".");
        return Double.parseDouble(sellRateText);
    }

    @Step
    public ConversionPage clickShowResultButton() {
        webDriver.findElement(By.className("rates-button")).click();
        return this;
    }

    @Step
    public void closeCookeWarningMessage() {
        webDriver.findElement(By.xpath("//*[@title='Закрыть предупреждение']")).click();
    }

    public double getConversionResultAmount() {
        Awaitility.await("Wait for conversion result").until(() -> !getConversionResultText().isEmpty());
        String resultText = getConversionResultText();
        resultText = resultText.substring(0, resultText.indexOf(",") + 3).replace(",", ".").replace(" ", "");
        return Double.parseDouble(resultText);
    }

    @Step
    private String getConversionResultText() {
        return webDriver.findElement(By.className("rates-converter-result__total-to")).getText();
    }

    @Step
    public Integer getScaleRate() {
        try {
            WebDriverManager.setImplicitlyWaitTimeout(1000);
            String scaleText = webDriver.findElement(By.xpath("//tr[@class='rates-current__table-row']/td[@class='rates-current__table-cell rates-current__table-cell_column_scale']"))
                    .getText();
            return new Integer(scaleText);
        } catch (NoSuchElementException e) {
            return 1;
        } finally {
            WebDriverManager.setDefaultImplicitlyWait();
        }
    }

    @Step
    public Currency getCurrentRateCurrency() {
        String currencyText = webDriver.findElement(By.xpath("//tr[@class='rates-current__table-row']/td[@class='rates-current__table-cell rates-current__table-cell_column_name']"))
                .getText();
        return Currency.valueOf(currencyText.split(" ")[0]);
    }

    @Step
    public ConversionPage selectInternetBankExchange() {
        webDriver.findElement(By.xpath("//div[text()='Интернет-банк']")).click();
        return this;
    }

    @Step
    public ConversionPage selectVspStationExchange() {
        webDriver.findElement(By.xpath("//div[text()='Отделение (ВСП)']")).click();
        return this;
    }
}
