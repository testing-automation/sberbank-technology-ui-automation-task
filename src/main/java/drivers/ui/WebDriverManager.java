package drivers.ui;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverManager {

    private static WebDriver webDriver;
    private static int defaultImplicitlyTimeout = 5;
    private static int defaultPageLoadTimeout = 10;

    public static WebDriver getWebDriver() {
        if (webDriver == null) {
            ChromeDriverManager.getInstance().setup();
            webDriver = new ChromeDriver();
            webDriver.manage().window().maximize();
            defaultPageLoadTimeout = 10;
            webDriver.manage().timeouts()
                    .implicitlyWait(defaultImplicitlyTimeout, TimeUnit.SECONDS)
                    .pageLoadTimeout(defaultPageLoadTimeout, TimeUnit.SECONDS);
        }
        return webDriver;
    }

    public static void closeWebDriver() {
        if (webDriver != null) webDriver.quit();
        webDriver = null;
    }


    public static void setDefaultImplicitlyWait() {
        webDriver.manage().timeouts().implicitlyWait(defaultImplicitlyTimeout, TimeUnit.SECONDS);
    }

    public static void setImplicitlyWaitTimeout(int milliseconds) {
        webDriver.manage().timeouts().implicitlyWait(milliseconds, TimeUnit.MILLISECONDS);
    }

}
