package converter;

import drivers.ui.WebDriverManager;
import drivers.ui.pages.ConversionPage;
import logic.Currency;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.assertj.core.api.Assertions.assertThat;


public class ConversionCalculatingTest {

    static ConversionPage conversionPage = new ConversionPage();

    @AfterAll
    static void closeBrowser() {
        WebDriverManager.closeWebDriver();
    }

    @BeforeAll
    static void openAndCloseCookieMessage(){
           conversionPage.open();
           conversionPage.closeCookeWarningMessage();
    }

    @BeforeEach
    void openPage() {
        conversionPage.open();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/currencies.csv")
    void correctOneStepConversionCalculating(Currency testingCurrency, double amountForConversion) {
        conversionPage.chooseFromCurrency(Currency.RUB)
                .chooseToCurrency(testingCurrency)
                .fillAmount(amountForConversion);

        double saleRate = conversionPage.getSaleRate();
        int scaleRate = conversionPage.getScaleRate();
        double saleConversionResult = conversionPage.clickShowResultButton()
                .getConversionResultAmount();

        assertThat(saleConversionResult).isEqualTo(roundToHundreds(scaleRate * amountForConversion / saleRate));

        double buyConversionResult = conversionPage.chooseFromCurrency(testingCurrency)
                .chooseToCurrency(Currency.RUB)
                .clickShowResultButton()
                .getConversionResultAmount();
        double buyRate = conversionPage.getBuyRate();
        scaleRate = conversionPage.getScaleRate();

        assertThat(buyConversionResult).isEqualTo(roundToHundreds(amountForConversion * buyRate / scaleRate));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/currencies.csv")
    void conversionCurrencyChangingShouldChangeRateCurrencies(Currency testingCurrency) throws Exception {
        conversionPage.chooseToCurrency(testingCurrency)
                .chooseFromCurrency(Currency.RUB);

        Currency rateCurrency = conversionPage.getCurrentRateCurrency();

        assertThat(rateCurrency).isEqualByComparingTo(testingCurrency);


        conversionPage.chooseToCurrency(Currency.RUB)
                .chooseFromCurrency(testingCurrency);

        rateCurrency = conversionPage.getCurrentRateCurrency();

        assertThat(rateCurrency).isEqualByComparingTo(testingCurrency);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/currencies.csv")
    void exchangeInVspStationIsLessProfitableForCustomer(Currency testingCurrency) throws Exception {
        conversionPage.chooseToCurrency(testingCurrency)
                .selectInternetBankExchange();

        double saleRate = conversionPage.getSaleRate();
        double buyRate = conversionPage.getBuyRate();

        conversionPage.selectVspStationExchange();

        double vspSaleRate = conversionPage.getSaleRate();
        double vspBuyRate = conversionPage.getBuyRate();

        assertThat(vspSaleRate).isGreaterThan(saleRate);
        assertThat(vspBuyRate).isLessThan(buyRate);
    }

    private double roundToHundreds(double value) {
        double scale = Math.pow(10, 2);
        return Math.round(value * scale) / scale;
    }

}
